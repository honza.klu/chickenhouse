import machine
import ads1x15
import ina219
import onewire
import ds18x20
import time
import json
import ubinascii
import network
import _thread as thread
import socket

import webinterface
import captivedns

#import ulogging as logging
import logging as logging

from slimDNS import SlimDNSServer
#import slimDNS
import picoweb

import webrepl

webrepl.start()

DATA_LOG_FN = "log.txt"
webinterface.data["DATA_LOG_FN"] = DATA_LOG_FN

class FileHandler(logging.Handler):
    def emit(self, data):
        with open(DATA_LOG_FN, "a+") as f:
            json_data = json.dumps({"datetime": time.localtime(), "data": data.__dict__})
            f.write(json_data)
            f.write(b"\n")

logger = logging.getLogger()
logger.level = logging.DEBUG
logging.Logger.handlers.append(FileHandler())

with open("settings.json", "rb") as f:
    settings = json.load(f)

print("LOG:")
try:
    with open(DATA_LOG_FN, "rt") as f:
        print("printing")
        for line in f:
            print(line)
except Exception as e:
    print("Printing log FAILED!")
    print(str(e))

DOOR_DIRS = {"down", "up"}

#Setup network
ap_if = network.WLAN(network.AP_IF)
ap_if.ifconfig(('4.3.2.1', '255.255.255.0', '0.0.0.0', '4.3.2.1'))
ap_if.config(essid="chicken_house", password="chicken_house",
             authmode=network.AUTH_WPA_WPA2_PSK
             )
ap_if.active(True)
local_addr = ap_if.ifconfig()[0]
print("LOCAL IP:", local_addr)

captivedns.local_addr = local_addr
captive_dns = captivedns.CaptiveDNS()
captive_dns.start()

logger.info("Starting web")
app = picoweb.WebApp(__name__, webinterface.ROUTES, default_handler=webinterface.not_found)
#app.run(debug=1, host="0.0.0.0")
picoweb_threadthread = thread.start_new_thread(app.run, ("0.0.0.0", 80, 1))

#Mdns
logger.info("Starting dns")

def app():
    door_target = "down"

    rtc = machine.RTC()
    print("wakeup", rtc.datetime())

    hall_up = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP)
    hall_down = machine.Pin(12, machine.Pin.IN, machine.Pin.PULL_UP)
    pwm_pin = machine.Pin(27, machine.Pin.OUT, None)
    pwm = machine.PWM(pwm_pin)
    pwm.freq(100)

    dalas_pin = machine.Pin(32)
    solenoid = machine.Pin(13, machine.Pin.OUT)

    ow = onewire.OneWire(dalas_pin)
    ds = ds18x20.DS18X20(ow)

    i2c = machine.I2C(scl=machine.Pin(25), sda=machine.Pin(26), freq=40000)
    adc = ads1x15.ADS1115(i2c)
    ina = ina219.INA219(0.100, i2c, 0.1)

    pwm.duty(round(1024//10*1.5))

    while True:
        pass

    first_iteration = True
    while True:
        can_sleep = True
        try:
            ds.convert_temp()
        except Exception as e:
            pass

        adc.gain = 5
        raw = adc.read(0)
        light_sens = adc.raw_to_v(raw)

        #adc.gain = 5
        #raw = adc.read(0)
        #motor_current = adc.raw_to_v(raw)

        battery_current = ina.current()
        battery_voltage = ina.voltage()

        time.sleep(0.750)
        try:
            temp = ds.read_temp(ubinascii.a2b_base64(settings["winch_temp_sensor"]))
        except Exception as e:
            temp = float("NaN")


        print(hall_up.value(), hall_down.value())
        print(light_sens, "\t", battery_current, battery_voltage, "\t", temp)
        if first_iteration:
            log_data({"log": "data_start", "light_sens": light_sens,
                      "battery_current": battery_current, "battery_voltage": battery_voltage,
                      "hall_up": hall_up.value(), "hall_down": hall_down.value()}
                     )
            first_iteration = False
        #Decide direction
        if light_sens < 0.03:
            door_target = "down"
            #pwm.duty(round(1024 // 10 * 1.0))
        elif light_sens > 0.05:
            door_target = "up"
            #pwm.duty(round(1024 // 10 * 2.0))
        else:
            #pwm.duty(round(1024 // 10 * 1.5))
            print("all ok")
        #Move to position
        if door_target == "down" and hall_down.value():
            pwm.duty(round(1024 // 10 * 0.5))
            solenoid.value(1)
            print("Moving down")
            can_sleep = False
        elif door_target == "up" and hall_up.value():
            pwm.duty(round(1024 // 10 * 2.5))
            print("Moving up")
            solenoid.value(1)
            can_sleep = False
        else:
            print("Stop")
            pwm.duty(round(1024 // 10 * 1.5))
            solenoid.value(0)
        #Sleep
        time.sleep(0.2)
        if can_sleep:
            print("going to sleep")
            log_data({"log": "data_end", "light_sens": light_sens,
                      "battery_current": battery_current, "battery_voltage": battery_voltage,
                      "hall_up": hall_up.value(),
                      "hall_down": hall_down.value()}
                     )
            log_data({"log": "deepsleep_start"})
            machine.deepsleep(int(settings["sleep_time"]*1000))