import machine
import time
import app
import usys

print("MAIN START")

def main():
  print("Type your code here!")

try:
  #run main app here to reset on unhandled exceptions
  app.app()
except Exception as e:
  print("Unhandled exception:%s" % (str(e),))
  usys.print_exception(e)
  print("Runtime error encountered. Restarting in 10s")
  time.sleep(10.0)
  print("Restarting now!")
  machine.reset()
print("MAIN END")