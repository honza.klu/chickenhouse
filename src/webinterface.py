import picoweb
import logging

logger = logging.getLogger()
logger.level = logging.DEBUG

data = {}

#picoweb
def bootstrap_head(req, resp):
    yield from resp.awrite("""
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    
        <title>Carousel Template for Bootstrap</title>
    
        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">
    
        <!-- Bootstrap core CSS -->
        <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
        <!-- Custom styles for this template -->
        <link href="carousel.css" rel="stylesheet">
    </head>    
    """)

def bootstrap_header(req, resp):
    yield from resp.awrite("""
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="">Chicken house</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Settings</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    """)

def index(req, resp):
    logger.info("Serving index")
    # You can construct an HTTP response completely yourself, having
    # a full control of headers sent...
    yield from resp.awrite("HTTP/1.0 200 OK\r\n")
    yield from resp.awrite("Content-Type: text/html\r\n")
    yield from resp.awrite("\r\n")
    yield from resp.awrite("<a href='logs'>logs</a><br/>")
    yield from resp.awrite("<a href='parameters'>parameters</a><br/>")
    yield from resp.awrite("<a href='client_info'>client_info</a><br/>")
    yield from resp.awrite("<a href='bootstrap/css/bootstrap.css'>bootstrap</a><br/>")


def logs(req, resp):
    logger.info("Serving logs")
    yield from resp.awrite("HTTP/1.0 200 OK\r\n")
    yield from resp.awrite("Content-Type: text/html\r\n")
    yield from resp.awrite("\r\n")
    with open(data.get("DATA_LOG_FN", None), "rt") as f:
        print("printing")
        for line in f:
            yield from resp.awrite(line)
            yield from resp.awrite("\r\n")
            yield from resp.awrite("<br/>")

def client_info(req, resp):
    yield from resp.awrite("HTTP/1.0 200 OK\r\n")
    yield from resp.awrite("Content-Type: text/html\r\n")
    yield from resp.awrite("\r\n")
    for k, v in req.headers.items():
        print("PRINTING HEADER:", k, ":", v)
        yield from resp.awrite("%s:%s" % (k, v))
        yield from resp.awrite("<br/>\r\n")

def settings(req, resp):
    yield from resp.awrite("HTTP/1.0 200 OK\r\n")
    yield from resp.awrite("Content-Type: text/html\r\n")
    yield from resp.awrite("\r\n")
    for k, v in req.headers.items():
        print("PRINTING HEADER:", k, ":", v)
        yield from resp.awrite("%s:%s" % (k, v))
        yield from resp.awrite("<br/>\r\n")

def not_found(req, resp):
    logger.info("Not found, redirecting" + str(req))
    yield from resp.awrite("HTTP/1.0 302 OK\r\n")
    yield from resp.awrite("Location: /\r\n")
    yield from resp.awrite("Content-Type: text/html; charset=us-ascii\r\n")
    yield from resp.awrite("\r\n")

ROUTES = [
    ("/", index),
    ("/index", index),
    ("/logs", logs),
    ("/client_info", client_info),
    ("/settings", settings),
]