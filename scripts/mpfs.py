import os
import logging
import hashlib

logger = logging.getLogger()

PORT="COM9"

CREATED = set()

class MpFs:
    def __init__(self, port):
        self.port = port

    def mkdir(self, path):
        path = str(path)
        path = path.split("/")
        if len(path)<=1:
            print("skip", path)
            return
        for i in range(1, len(path)-1):
            p = "/".join(path[0:i+1])
            cmd = "ampy --port %s mkdir %s" % (self.port, p)
            print(cmd)
            ret = os.system(cmd)
            if ret==1: #ignore if dir already exist
                pass
            elif ret:
                raise RuntimeError("mkdir failed (ret=%d, path=%s)" % (ret, p))

    def upload(self, path, base=None, ignore_hash=None):
        uploaded_hashes = set()
        if base is None:
            base=path
        for fn in os.listdir(path):
            spath = os.path.join(path, fn)
            if os.path.isdir(spath):
                hash = hashlib.sha1("mkdir".encode('utf8') + spath.encode('utf8')).digest()
                if hash not in ignore_hash:
                    hs = self.upload(spath, base=base, ignore_hash=ignore_hash)
                    uploaded_hashes.update(hs)
                    ignore_hash.add(hash)
            else:
                hash = (hashlib.sha1(open(spath, "rb").read()).digest(), path, fn)
                if ignore_hash and (hash in ignore_hash):
                    logger.debug("Skipping %s, it was already uploaded last time")
                    uploaded_hashes.add(hash)
                    continue
                logger.debug("uploading:", spath)
                dest = spath[len(base):].replace("\\", "/")
                if dest not in CREATED:
                    self.mkdir(dest)
                    CREATED.add(dest)
                cmd = "ampy --port %s put %s %s" % (self.port, spath, dest)
                print("CMD:", cmd)
                ret = os.system(cmd)
                logger.debug("RET:", ret)
                if ret:
                    raise RuntimeError("put failed")
                uploaded_hashes.add(hash)
        return uploaded_hashes