import pickle
import yaml
import argparse

import mpfs as mpfs

settings = yaml.load(open("settings.yaml", "rt"))

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--upload_all', '-f', action='store_true',
                    help='upload all files')
args = parser.parse_args()

ignore_hashes = {}
try:
    with open("hashes.pickle", "rb") as f:
        ignore_hashes = pickle.load(f)
except (EOFError, FileNotFoundError) as e:
    print("Loading hashes failed")
if args.upload_all:
    ignore_hashes = {}
print(ignore_hashes)

mpfs = mpfs.MpFs(settings["port"])
uploaded_hashes = mpfs.upload("src", ignore_hash=ignore_hashes)

print(uploaded_hashes)

with open("hashes.pickle", "wb") as f:
    pickle.dump(uploaded_hashes, f)