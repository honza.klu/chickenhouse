import os
import gzip

PATHS = [(r"static_comp", r"src\app\static")]

def compres_path(src, dest, append_ext=".gz_static"):
    for s_fn in os.listdir(src):
        s_path = os.path.join(src, s_fn)
        d_path = os.path.join(dest, s_fn)
        print(s_path)
        if os.path.isdir(s_path):
            print("DIR")
            try:
                os.mkdir(d_path)
            except FileExistsError as e:
                pass
            compres_path(s_path, d_path)
        else:
            print("FILE")
            with open(s_path, "rb") as sf, gzip.open(d_path + append_ext, "wb") as df:
                df.write(sf.read())

for path in PATHS:
    compres_path(path[0], path[1])



