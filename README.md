esptool.py --port COM9 erase_flash
esptool.py --chip esp32 --port COM9 write_flash -z 0x1000 esp32-



import machine
import ads1x15
import ina219
import onewire
import ds18x20
import time
import json
import ubinascii

dalas_pin = machine.Pin(32)
ow = onewire.OneWire(dalas_pin)
ds = ds18x20.DS18X20(ow)
ds.scan()